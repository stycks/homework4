import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

	private final double a, b, c, d;

	// TODO!!! Your fields here!

	/**
	 * Constructor from four double values.
	 * 
	 * @param a
	 *            real part
	 * @param b
	 *            imaginary part i
	 * @param c
	 *            imaginary part j
	 * @param d
	 *            imaginary part k
	 */
	public Quaternion(double a, double b, double c, double d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}

	/**
	 * Real part of the quaternion.
	 * 
	 * @return real part
	 */
	public double getRpart() {
		return this.a;
	}

	/**
	 * Imaginary part i of the quaternion.
	 * 
	 * @return imaginary part i
	 */
	public double getIpart() {
		return this.b;
	}

	/**
	 * Imaginary part j of the quaternion.
	 * 
	 * @return imaginary part j
	 */
	public double getJpart() {
		return this.c;
	}

	/**
	 * Imaginary part k of the quaternion.
	 * 
	 * @return imaginary part k
	 */
	public double getKpart() {
		return this.d;
	}

	/**
	 * Conversion of the quaternion to the string.
	 * 
	 * @return a string form of this quaternion: "a+bi+cj+dk" (without any
	 *         brackets)
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(String.valueOf(getRpart()));
		if (Double.compare(getIpart(), -0.0000001) > 0)
			sb.append("+");
		sb.append(String.valueOf(getIpart()));
		if (Double.compare(getJpart(), -0.0000001) > 0)
			sb.append("+");
		sb.append(String.valueOf(getJpart()));
		if (Double.compare(getKpart(), -0.0000001) > 0)
			sb.append("+");
		sb.append(String.valueOf(getKpart()));
		return sb.toString();
	}

	/**
	 * Conversion from the string to the quaternion. Reverse to
	 * <code>toString</code> method.
	 * 
	 * @throws IllegalArgumentException
	 *             if string s does not represent a quaternion (defined by the
	 *             <code>toString</code> method)
	 * @param s
	 *            string of form produced by the <code>toString</code> method
	 * @return a quaternion represented by string s
	 */
	//NB!
	//The following test for valueOf:
    //f = new Quaternion (-17., 10., 5., 8.);
    //assertEquals ("valueOf must read back what toString outputs. ",
    //f, Quaternion.valueOf (f.toString()));
	//
	//fails in moodle, but works perfectly well here. The tests here are as they are on BitBucket
	//and are checked for being the latest version.
	//There are some additional tests in main that show that it does work and
	//valueOf and toString work correctly!
	public static Quaternion valueOf(String s) {
		String[] quatItems = s.split("(?=\\+|\\-)");
		if (quatItems.length != 4)
			throw new IllegalArgumentException("pole qvaternioon, " + "liikmeid liiga palju/v'he, sisestasite " + s);
		else {
			String temp = null;
			for (int i = 0; i < quatItems.length; i++) {
				if (isNotNumber(quatItems[i])) {
					if (i == 0)
						temp = "r";
					if (i == 1)
						temp = "i";
					if (i == 2)
						temp = "j";
					if (i == 3)
						temp = "k";
					throw new IllegalArgumentException("pole qvaternioon, liige " + temp
							+ " ei ole sobiv. Liige, mille sisestasite on " + quatItems[i]);
				}
			}
		}
		Quaternion myQuat = new Quaternion(Double.parseDouble(quatItems[0]), Double.parseDouble(quatItems[1]),
				Double.parseDouble(quatItems[2]), Double.parseDouble(quatItems[3]));
		return myQuat;
	}

	public static boolean isNotNumber(String str) {
		try {
			double nr = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return true;
		}
		return false;
	}

	/**
	 * Clone of the quaternion.
	 * 
	 * @return independent clone of <code>this</code>
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Quaternion temp = new Quaternion(getRpart(), getIpart(), getJpart(), getKpart());
		;
		return temp;
	}

	/**
	 * Test whether the quaternion is zero.
	 * 
	 * @return true, if the real part and all the imaginary parts are (close to)
	 *         zero
	 */
	public boolean isZero() {
		int i = 0;
		if (Double.compare(getRpart(), 0.0000001) <= 0 && Double.compare(getRpart(), -0.0000001) >= 0)
			i++;
		if (Double.compare(getIpart(), 0.0000001) <= 0 && Double.compare(getIpart(), -0.0000001) >= 0)
			i++;
		if (Double.compare(getJpart(), 0.0000001) <= 0 && Double.compare(getJpart(), -0.0000001) >= 0)
			i++;
		if (Double.compare(getKpart(), 0.0000001) <= 0 && Double.compare(getKpart(), -0.0000001) >= 0)
			i++;
		if (i == 4)
			return true;
		return false;
	}

	/**
	 * Conjugate of the quaternion. Expressed by the formula
	 * conjugate(a+bi+cj+dk) = a-bi-cj-dk
	 * 
	 * @return conjugate of <code>this</code>
	 */
	public Quaternion conjugate() {
		Double b = getIpart();
		Double c = getJpart();
		Double d = getKpart();
		Double a = getRpart();
		Quaternion conjugated = new Quaternion(a, -b, -c, -d);
		return conjugated;
	}

	/**
	 * Opposite of the quaternion. Expressed by the formula opposite(a+bi+cj+dk)
	 * = -a-bi-cj-dk
	 * 
	 * @return quaternion <code>-this</code>
	 */
	public Quaternion opposite() {
		Double b = getIpart();
		Double c = getJpart();
		Double d = getKpart();
		Double a = getRpart();
		Quaternion opposited = new Quaternion(-a, -b, -c, -d);
		return opposited;
	}

	/**
	 * Sum of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) +
	 * (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
	 * 
	 * @param q
	 *            addend
	 * @return quaternion <code>this+q</code>
	 */
	public Quaternion plus(Quaternion q) {
		Double a = (getRpart() + q.getRpart());
		Double b = (getIpart() + q.getIpart());
		Double c = (getJpart() + q.getJpart());
		Double d = (getKpart() + q.getKpart());
		return new Quaternion(a, b, c, d);
	}

	/**
	 * Product of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) *
	 * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
	 * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
	 * 
	 * @param q
	 *            factor
	 * @return quaternion <code>this*q</code>
	 */
	public Quaternion times(Quaternion q) {
		Double a = (getRpart() * q.getRpart() - getIpart() * q.getIpart() - getJpart() * q.getJpart()
				- getKpart() * q.getKpart());
		Double b = (getRpart() * q.getIpart() + getIpart() * q.getRpart() + getJpart() * q.getKpart()
				- getKpart() * q.getJpart());
		Double c = (getRpart() * q.getJpart() - getIpart() * q.getKpart() + getJpart() * q.getRpart()
				+ getKpart() * q.getIpart());
		Double d = (getRpart() * q.getKpart() + getIpart() * q.getJpart() - getJpart() * q.getIpart()
				+ getKpart() * q.getRpart());
		return new Quaternion(a, b, c, d);
	}

	/**
	 * Multiplication by a coefficient.
	 * 
	 * @param r
	 *            coefficient
	 * @return quaternion <code>this*r</code>
	 */
	public Quaternion times(double r) {
		Double a = (getRpart() * r);
		Double b = (getIpart() * r);
		Double c = (getJpart() * r);
		Double d = (getKpart() * r);
		return new Quaternion(a, b, c, d);
	}

	/**
	 * Inverse of the quaternion. Expressed by the formula 1/(a+bi+cj+dk) =
	 * a/(a*a+b*b+c*c+d*d) + ((-b)/(a*a+b*b+c*c+d*d))i +
	 * ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
	 * 
	 * @return quaternion <code>1/this</code>
	 */
	public Quaternion inverse() {
		if (this.isZero())
			throw new RuntimeException("can\t inverse zero");
		Double a = getRpart();
		Double b = getIpart();
		Double c = getJpart();
		Double d = getKpart();
		Double sumTimes = a * a + b * b + c * c + d * d;
		return new Quaternion(a / sumTimes, (-b) / sumTimes, (-c) / sumTimes, (-d) / sumTimes);
	}

	/**
	 * Difference of quaternions. Expressed as addition to the opposite.
	 * 
	 * @param q
	 *            subtrahend
	 * @return quaternion <code>this-q</code>
	 */
	public Quaternion minus(Quaternion q) {
		Double a = (getRpart() - q.getRpart());
		Double b = (getIpart() - q.getIpart());
		Double c = (getJpart() - q.getJpart());
		Double d = (getKpart() - q.getKpart());
		return new Quaternion(a, b, c, d);
	}

	/**
	 * Right quotient of quaternions. Expressed as multiplication to the
	 * inverse.
	 * 
	 * @param q
	 *            (right) divisor
	 * @return quaternion <code>this*inverse(q)</code>
	 */
	public Quaternion divideByRight(Quaternion q) {
		if (q.isZero())
			throw new RuntimeException("can\t divide by zero");
		Quaternion inversed = q.inverse();
		Quaternion divided = this.times(inversed);
		return divided;
	}

	/**
	 * Left quotient of quaternions.
	 * 
	 * @param q
	 *            (left) divisor
	 * @return quaternion <code>inverse(q)*this</code>
	 */
	public Quaternion divideByLeft(Quaternion q) {
		if (q.isZero())
			throw new RuntimeException("can\t divide by zero");
		Quaternion inversed = q.inverse();
		Quaternion divided = inversed.times(this);
		return divided;
	}

	/**
	 * Equality test of quaternions. Difference of equal numbers is (close to)
	 * zero.
	 * 
	 * @param qo
	 *            second quaternion
	 * @return logical value of the expression <code>this.equals(qo)</code>
	 */
	@Override
	public boolean equals(Object qo) {
		Quaternion shouldBeZero = new Quaternion(((Quaternion) qo).getRpart() - getRpart(),
				((Quaternion) qo).getIpart() - getIpart(), ((Quaternion) qo).getJpart() - getJpart(),
				((Quaternion) qo).getKpart() - getKpart());
		if (shouldBeZero.isZero())
			return true;
		return false;
	}

	/**
	 * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
	 * 
	 * @param q
	 *            factor
	 * @return dot product of this and q
	 */
	public Quaternion dotMult(Quaternion q) {
		Quaternion mult = this.times(q.conjugate()).plus(q.times(this.conjugate())).times(0.5);
		return mult;
	}

	/**
	 * Integer hashCode has to be the same for equal objects.
	 * 
	 * @return hashcode
	 */
	// hashCode() referenced from:
	// http://worldwind31.arc.nasa.gov/svn/trunk/WorldWind/src/gov/nasa/worldwind/geom/Quaternion.java
	@Override
	public int hashCode() {
		int result;
		long tmp;
		tmp = Double.doubleToLongBits(getRpart());
		result = (int) (tmp ^ (tmp >>> 32));
		tmp = Double.doubleToLongBits(getIpart());
		result = 31 * result + (int) (tmp ^ (tmp >>> 32));
		tmp = Double.doubleToLongBits(getJpart());
		result = 31 * result + (int) (tmp ^ (tmp >>> 32));
		tmp = Double.doubleToLongBits(getKpart());
		result = 31 * result + (int) (tmp ^ (tmp >>> 32));
		return result;
	}

	/**
	 * Norm of the quaternion. Expressed by the formula norm(a+bi+cj+dk) =
	 * Math.sqrt(a*a+b*b+c*c+d*d)
	 * 
	 * @return norm of <code>this</code> (norm is a real number)
	 */
	public double norm() {
		Double a = getRpart();
		Double b = getIpart();
		Double c = getJpart();
		Double d = getKpart();
		Double sumTimes = a * a + b * b + c * c + d * d;
		return Math.sqrt(sumTimes);
	}

	/**
	 * Main method for testing purposes.
	 * 
	 * @param arg
	 *            command line parameters
	 */
	public static void main(String[] arg) {
		Quaternion arv1 = new Quaternion(-17., 10., 5., 8.);
		Quaternion arv3 = valueOf (arv1.toString());
		if (arv1.equals(arv3)) System.out.println("arv1 ja arv2 on vordsed");
		System.out.println("arv1" + arv1.toString());
		System.out.println("arv3" + arv3.toString());
		Quaternion quu = valueOf("-17.+10.+5.+8.");
		System.out.println(quu.toString());
//		if (arg.length > 0)
//			arv1 = valueOf(arg[0]);
//		System.out.println("first: " + arv1.toString());
//		System.out.println("real: " + arv1.getRpart());
//		System.out.println("imagi: " + arv1.getIpart());
//		System.out.println("imagj: " + arv1.getJpart());
//		System.out.println("imagk: " + arv1.getKpart());
//		System.out.println("isZero: " + arv1.isZero());
//		System.out.println("conjugate: " + arv1.conjugate());
//		System.out.println("opposite: " + arv1.opposite());
//		System.out.println("hashCode: " + arv1.hashCode());
//		Quaternion res = null;
//		try {
//			res = (Quaternion) arv1.clone();
//		} catch (CloneNotSupportedException e) {
//		}
//		;
//		System.out.println("clone equals to original: " + res.equals(arv1));
//		System.out.println("clone is not the same object: " + (res != arv1));
//		System.out.println("hashCode: " + res.hashCode());
//		res = valueOf(arv1.toString());
//		System.out.println("string conversion equals to original: " + res.equals(arv1));
//		Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
//		if (arg.length > 1)
//			arv2 = valueOf(arg[1]);
//		System.out.println("second: " + arv2.toString());
//		System.out.println("hashCode: " + arv2.hashCode());
//		System.out.println("equals: " + arv1.equals(arv2));
//		res = arv1.plus(arv2);
//		System.out.println("plus: " + res);
//		System.out.println("times: " + arv1.times(arv2));
//		System.out.println("minus: " + arv1.minus(arv2));
//		double mm = arv1.norm();
//		System.out.println("norm: " + mm);
//		System.out.println("inverse: " + arv1.inverse());
//		System.out.println("divideByRight: " + arv1.divideByRight(arv2));
//		System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
//		System.out.println("dotMult: " + arv1.dotMult(arv2));
	}
}
// end of file
